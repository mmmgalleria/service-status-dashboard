pipeline {
    agent any

    environment {
        PackageGroup = ""
        PackageName = "aws-devicefarm-config"
        PackageBranch = "main"
        GitURL = "https://gitlab.com/mmmgalleria"
        GitCredentials = ""
    }

    stages {
        stage('Checkout') {
            steps {
                script {
                    print(env.SourceCodeBranch)
                     withCredentials([usernamePassword(credentialsId: env.GitCredentials, passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                        sh("""
                            git config --global credential.helper "!f() { echo username=\\$GIT_USERNAME; echo password=\\$GIT_PASSWORD; }; f"
                        """)
                            
                        try {
                            sh 'git remote prune origin'
                            sh 'git fetch --tags --force'
                            sh 'git clean -xffd && git submodule foreach --recursive git clean -xffd'
                            sh 'git fetch --all --prune'
                        }
                        catch (Exception e) { }
                        finally {
                            scmVars = checkout changelog: true, poll: true, scm: [
                            $class: 'GitSCM',
                            branches: [[name: "origin/${env.PackageBranch}"]],
                            doGenerateSubmoduleConfigurations: false,
                            extensions: [
                            ],
                            submoduleCfg: [],
                            userRemoteConfigs: [
                                [
                                credentialsId: env.GitCredentials,
                                name: 'origin',
                                refspec: "+refs/heads/${env.PackageBranch}:refs/remotes/origin/${env.PackageBranch}",
                                url: "${env.GitURL}/${env.PackageGroup}/${env.PackageName}.git"
                                ]
                            ]
                        ]
                        }
                    }
                }
            } 
        }

        stage("Install Dependency") {
            steps {
                script {
                    sh "pip install django==2.1.15"
                }
            }
        }

        stage('Build Django') {
            steps {
                script {
                    sh "BUILD_ID=dontKillMe nohup python3 manage.py runserver 0.0.0.0:8000 &"
                    sh "curl -k 0.0.0.0:8000/status/"
                }
            }
        }
    }
}