from django.shortcuts import render
from django.http import HttpResponse
import json
import os
import requests

# Create your views here.
def table(request):
    group_name = get_parameter(request)
    data = read_json_file()
    groups = get_all_group_name(data)
    output = get_request_status(data, group_name)
    context = {"groups": groups, "data": output}
    return render(request, 'template.html', context)

def get_all_group_name(data):
    return [{"name": d['name'], "url": "/status/?group=%s"%d['name'].replace(" ","-")} for d in data]

def get_request_status(data, group_name):
    for group in data:
        services = group['services']
        for s in services:
            s = sent_request(s)
        if group['name'] == group_name.replace("-", " "):
            output = [group]
            break
        else:
            output = data
    return output

def get_parameter(request):
    query = dict(request.GET)
    group_name = ""
    if "group" in query:
        group_name = str(query['group'][0])
    return group_name

def read_json_file():
    path = os.getcwd()
    f = open(os.path.join(path, 'data/data/test.json'))
    data = json.load(f)
    return data

def sent_request(data):
    payload = None
    if data['body'] is not None:
        payload = json.dumps(data['body'])
    try:
        if data['method']    == 'GET':
            response = requests.get(data['url'], data=payload, headers=data['headers'], verify=False)
        elif data['method'] == 'POST':
            response = requests.post(data['url'], data=payload, headers=data['headers'], verify=False)
        elif data['method'] == 'PUT':
            response = requests.put(data['url'], data=payload, headers=data['headers'], verify=False)
        elif data['method'] == 'DELETE':
            response = requests.delete(data['url'], data=payload, headers=data['headers'], verify=False)
        elif data['method'] == 'PATCH':
            response = requests.patch(data['url'], data=payload, headers=data['headers'], verify=False)
        if response.status_code == data['expected_status'] and response.json()['status']==data['expected_response']['status']:
            data['status'] = 'PASSED'
        else:
            data['status'] = 'FAILED'
    except:
        data['status'] = 'FAILED'
    return data
