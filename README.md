# Service-Status-Dashboard

This repo using Django to show the Dashboard of Each Service. This page load data from JSON file and sent request to check the service status. After user visit this website, it will auto-refresh every minute.
## Prerequisites
Before you begin, ensure that you have the following software installed on your system:
- Python: Make sure you have Python installed.
### Install Django: 
With your virtual environment activated, install Django using pip:
```
pip install Django
```

## Run Server by this command
Start the development server to verify that your project is set up correctly. Run the following command:

```
python manage.py runserver
```
This command will start the development server, and you should see output indicating that the server is running. By default, it will run on http://localhost:8000.

## Access the Django admin interface: 
Open your web browser and visit http://localhost:8000/status. If everything is set up correctly, you should see the Django administration login page.



# Project Structure
Once you have set up your Django project, it will have the following structure:
```
myproject/        # Main project directory
    manage.py     # A command-line utility for executing various Django commands
    myproject/    # The actual Django project package
        __init__.py
        settings.py   # Project settings and configurations
        urls.py       # URL configurations for the project
        wsgi.py       # WSGI application entry point

```
The main project directory (myproject/) contains the manage.py script, which is used to interact with your Django project. The inner myproject/ directory is the Python package that represents your Django project. It contains the project's settings, URL configurations, and other necessary files.

You can create additional directories and files within your project to organize your code, templates, static files, and other resources as your project